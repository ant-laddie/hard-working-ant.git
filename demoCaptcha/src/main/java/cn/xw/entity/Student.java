package cn.xw.entity;

import lombok.Data;

/**
 * @author AnHui OuYang
 * @version 1.0
 */
@Data
public class Student {
    private Long id;            // 学生ID
    private String name;        // 学生姓名
    private Integer age;        // 学生年龄
    private String address;     // 学生地址
    private Double fraction;    // 学生分数

    public Student(Long id, String name, Integer age, String address, Double fraction) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
        this.fraction = fraction;
    }
}
