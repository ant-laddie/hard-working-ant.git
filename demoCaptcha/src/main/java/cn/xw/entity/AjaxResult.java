package cn.xw.entity;

import lombok.Data;

/**
 * 响应对象模型定义
 *
 * @author AnHui OuYang
 * @version 1.0
 */
@Data
public class AjaxResult<T> {
    private Integer code;   // 响应码
    private String msg;     // 响应信息
    private T data;         // 响应数据

    public AjaxResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
