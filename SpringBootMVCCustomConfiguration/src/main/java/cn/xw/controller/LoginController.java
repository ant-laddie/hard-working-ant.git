package cn.xw.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录Controller
 *
 * @author Anhui OuYang
 * @version 1.0
 **/
@RestController
public class LoginController {

    /***
     * 登录方法
     * @return String
     */
    @GetMapping("/login")
    public String login() {
        System.out.println("登录成功....");
        return "{'code':200,'msg':'登录成功'}";
    }

}
