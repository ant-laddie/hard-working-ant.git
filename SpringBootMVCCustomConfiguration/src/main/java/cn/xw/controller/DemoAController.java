package cn.xw.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Anhui OuYang
 * @version 1.0
 **/
@RestController
@RequestMapping("/demo")
public class DemoAController {

    /***
     * GetA请求测试
     * @return String
     */
    @GetMapping("/getTestA")
    public String getDemoA() {
        System.out.println("执行getDemoA()方法");
        return "{'code':200,'msg':'成功'}";
    }

    /***
     * GetA请求测试
     * @return String
     */
    @GetMapping("/getTestB")
    public String getDemoB() {
        System.out.println("执行getDemoB()方法");
        return "{'code':200,'msg':'成功'}";
    }

}
