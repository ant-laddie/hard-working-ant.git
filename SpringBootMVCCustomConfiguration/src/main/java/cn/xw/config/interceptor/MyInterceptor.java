package cn.xw.config.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 这里我自定义了一个拦截器的拦截规范，后面需要配置到WebMvcConfigurer
 * 我们可以配置多个拦截器，到时候全部配置到WebMvcConfigurer里
 * 注：这里我定义的拦截器就当权限权限路径拦截（具体项目里我们可以起一个见名知意的拦截器 如：PermissionInterceptor）
 *
 * @author Anhui OuYang
 * @version 1.0
 **/
@Component  // 加载Bean容器里
public class MyInterceptor implements HandlerInterceptor {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /***
     * 在请求处理前进行调用（Controller方法调用之前）
     * 可以在这一阶段进行全局权限校验等操作
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        log.info("【preHandle】在请求处理前进行调用，自定义拦截器被执行。。。。");
        // 这下面我们可以进行权限校验，校验token操作.....
        // .....
        //上面的代码执行完，若可以放行本次请求则一定要返回true，这样才会到达Controller
        return true;
    }

    /***
     * 请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）
     * 可以在这个阶段来操作 ModelAndView
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        log.info("【postHandle】请求处理之后进行调用，自定义拦截器被执行。。。。");
    }

    /***
     * 在整个请求结束之后被调用，也就是在DispatcherServlet渲染了对应的视图之后执行，主要用于资源清理工作。
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object handler, Exception ex) throws Exception {
        log.info("【afterCompletion】在整个请求结束之后被调用，自定义拦截器被执行。。。。");
    }
}

