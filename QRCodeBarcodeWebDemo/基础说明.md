## 使用说明:
    二维码生成工具是对 google.zxing 的包进行的一下封装，提供了二维码和条形码的生成，以及二维码的解析
## 方法说明：
    String decodeQRCode(File codeFile)
    String decodeQRCode(InputStream codeInput)
    Result decodeQRCode(BufferedImage bufferedImage)
    说明：上面三个方法是用来解析二维码的，注意的是只能传File、InputStream、BufferedImage三种类型

    void encodeQRCode(String content, HttpServletResponse response)
    void encodeQRCode(String content, String logoPath, HttpServletResponse response)
    void encodeQRCode(String content, File logoFile, HttpServletResponse response)
    void encodeQRCode(String content, InputStream logoInput, HttpServletResponse response)
    说明：上面四个方法是用来生成二维码的，
        参数1：生成二维码的信息
        参数2：生成二维码中间的Logo图标信息
        参数3：用来写出到前端页面的响应对象
        注意：第一个方法有2个参数，它没生成的是不带Logo图标的

    void encodeBarcode(String content, HttpServletResponse response)
    说明：这个方法用来生成条形码并响应到前端

    void setResponseHeaderInfoDownload(HttpServletResponse response)
    注意：这个是设置响应头信息的，若调用这个方法则代表当前响应到前端的二维码以下载的方式下载图片

## 配置说明：
    其实一般我们使用默认的配置就可以了，但是我们要调整二维码的生成属性则使用当前这个类，其中有几个重要配置
    errorCorrection：容错等级，若创建带Logo图标的二维码我们一般选择M、Q、H
    checkQRCode：生成的二维码是否需要校验其完整性，若校验失败可以调整宽高

