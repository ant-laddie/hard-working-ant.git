package cn.xw;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QrCodeBarcodeWebDemoApplication {

    public static void main(String[] args) {

        SpringApplication.run(QrCodeBarcodeWebDemoApplication.class, args);
    }

}
