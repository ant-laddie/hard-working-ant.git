package cn.xw.controller;

import cn.xw.utils.qrcode.QRCodeProperties;
import cn.xw.utils.qrcode.GenerateQRCode;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * 测试二维码控制器
 * @author Anhui OuYang
 * @version 1.0
 **/
@Slf4j
@Controller
@RequestMapping("/qrcode")
public class QRCodeController {

    /***
     * 生成二维码 如调用地址：http://localhost:8881/test/qrcode/getQRCode?qrMsg=hello
     * 我们可以在启动项目后执行 http://localhost:8881/test
     * @param qrMsg 二维码信息
     */
    @GetMapping("/getQRCode")
    public void getQRCode(@RequestParam(value = "qrMsg") String qrMsg, HttpServletResponse response) {
        //获取当前项目的路径根目录和Logo图标地址
        String path = Objects.requireNonNull(QRCodeController
                .class.getResource("/static/logo.gif")).getPath();
        File file = new File(path);

        //创建自定义的二维码生成属性（其实一般默认即可）
        QRCodeProperties qrCodeProperties = new QRCodeProperties();
        qrCodeProperties.setFormatName(QRCodeProperties.ImageType.GIF);
        qrCodeProperties.setErrorCorrection(ErrorCorrectionLevel.H);
        qrCodeProperties.setCodeWidth(120);
        qrCodeProperties.setCodeHeight(120);
        GenerateQRCode generateQRCode = new GenerateQRCode();
        // 设置属性
        generateQRCode.setProperties(qrCodeProperties);
        // 调用则代表直接以文件的方式在前端下载
        // generateQRCode.setResponseHeaderInfoDownload(response);
        generateQRCode.encodeQRCode(qrMsg, file, response);
    }
}
