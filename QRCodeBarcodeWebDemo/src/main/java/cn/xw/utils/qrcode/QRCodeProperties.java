package cn.xw.utils.qrcode;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 二维码和条形码的一些属性配置
 *
 * @author Anhui OuYang
 * @version 1.0
 **/
@Data
@NoArgsConstructor
public class QRCodeProperties {

    // ====================================== 基本属性 ======================================
    private Integer codeWidth = 300;              // 二维码宽度，单位像素
    private Integer codeHeight = 300;             // 二维码高度，单位像素
    private Integer barCodeWidth = 300;           // 条形码宽度，单位像素
    private Integer barCodeHeight = 150;          // 条形码高度，单位像素
    private Integer frontColor = 0x000000;        // 二维码前景色，0x000000 表示黑色
    private Integer backgroundColor = 0xFFFFFF;   // 二维码背景色，0xFFFFFF 表示白色

    // ================================= 生成二维码的配置属性 =================================
    private String charset = "UTF-8";                   // 生成二维码所采用的字符集内容编码
    private ImageType formatName = ImageType.PNG;       // 生成的二维码或者条形码格式
    private ErrorCorrectionLevel errorCorrection = ErrorCorrectionLevel.M; // 二维码的容错等级
    private Integer margin = 1;                   // 二维码的外边距大小，即二维码与图像边缘之间的空白区域的大小。

    // ====================================== 其它属性 ======================================
    private Boolean checkQRCode = true;     // 是否检查生成的二维码是否有效（只针对二维码）
    public static final BarcodeFormat qrcodeType = BarcodeFormat.QR_CODE; // 生成的二维码类型
    public static final BarcodeFormat barcodeType = BarcodeFormat.EAN_13; // 生成的条形码类型

    /***
     * 一些图片类型
     */
    @Getter
    public enum ImageType {
        JPG("jpg", "image/jpeg"),
        PNG("png", "image/png"),
        GIF("gif", "image/gif");

        //图片类型
        private final String type;
        private final String mineType;

        ImageType(String type, String mineType) {
            this.type = type;
            this.mineType = mineType;
        }
    }
}
