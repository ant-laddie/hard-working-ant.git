package cn.xw.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-17 17:10
 * RabbitMQ配置类
 */
@Configuration
public class RabbitMQConfig {

    //直接交换机
    public static final String CONFIRM_EXCHANGE = "confirmExchange";
    //队列
    public static final String CONFIRM_QUEUE = "confirmQueue";
    //绑定路由key
    public static final String CONFIRM_ROUTING_KEY = "confirmRoutingKey";

    /***
     * 创建交换机消息
     * @return Exchange
     */
    @Bean("confirmExchange")
    public Exchange confirmExchange() {
        return ExchangeBuilder.directExchange(CONFIRM_EXCHANGE).durable(true).build();
    }

    /***
     * 队列名称
     * @return Queue
     */
    @Bean("confirmQueue")
    public Queue confirmQueue() {
        return QueueBuilder.durable(CONFIRM_QUEUE).build();
    }

    /***
     * 绑定关系
     * @param confirmExchange 交换机消息
     * @param confirmQueue 队列消息
     * @return Binding
     */
    @Bean("delayedQueueBindDelayedExchange")
    public Binding delayedQueueBindDelayedExchange(@Qualifier(value = "confirmExchange") Exchange confirmExchange,
                                                   @Qualifier(value = "confirmQueue") Queue confirmQueue) {
        return BindingBuilder.bind(confirmQueue).to(confirmExchange).with(CONFIRM_ROUTING_KEY).noargs();
    }
}
