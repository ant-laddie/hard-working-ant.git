package cn.xw.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-16 23:10
 * 主题交换机配置类
 */
@Configuration
public class RabbitMQConfig {

    //交换机名称
    public static final String TOPIC_EXCHANGE = "TopicExchange";
    //队列Q1名称
    public static final String Q1 = "Q1Queue";
    //队列Q2名称
    public static final String Q2 = "Q2Queue";
    //路由绑定关系 Routing Key
    public static final String Q1_KEY = "*.orange.*";
    //路由绑定关系 Routing Key 1
    public static final String Q2_KEY_A = "*.*.rabbit";
    //路由绑定关系 Routing Key 2
    public static final String Q2_KEY_B = "lazy.#";

    /***
     * 主题交换机
     * @return Exchange
     */
    @Bean("topicExchange")
    public Exchange topicExchange() {
        return ExchangeBuilder.topicExchange(TOPIC_EXCHANGE).durable(true).build();
    }

    /***
     * 队列1信息
     * @return Queue
     */
    @Bean("q1Queue")
    public Queue q1Queue() {
        return QueueBuilder.durable(Q1).build();
    }

    /***
     * 队列2信息
     * @return Queue
     */
    @Bean("q2Queue")
    public Queue q2Queue() {
        return QueueBuilder.durable(Q2).build();
    }

    /***
     * 绑定关系，Q1Queue队列绑定的匹配路由为*.orange.*
     * @param topicExchange 交换机
     * @param q1Queue 队列1
     * @return Binding
     */
    @Bean("bindingA")
    public Binding bindingA(@Qualifier("topicExchange") Exchange topicExchange,
                            @Qualifier("q1Queue") Queue q1Queue) {
        return BindingBuilder.bind(q1Queue).to(topicExchange).with(Q1_KEY).noargs();
    }

    /***
     * 绑定关系，Q2Queue队列绑定的匹配路由为*.*.rabbit
     * @param topicExchange 交换机
     * @param q2Queue 队列2
     * @return Binding
     */
    @Bean("bindingB1")
    public Binding bindingB1(@Qualifier("topicExchange") Exchange topicExchange,
                             @Qualifier("q2Queue") Queue q2Queue) {
        return BindingBuilder.bind(q2Queue).to(topicExchange).with(Q2_KEY_A).noargs();
    }

    /***
     * 绑定关系，Q2Queue队列绑定的匹配路由为lazy.#
     * @param topicExchange 交换机
     * @param q2Queue 队列2
     * @return Binding
     */
    @Bean("bindingB2")
    public Binding bindingB2(@Qualifier("topicExchange") Exchange topicExchange,
                             @Qualifier("q2Queue") Queue q2Queue) {
        return BindingBuilder.bind(q2Queue).to(topicExchange).with(Q2_KEY_B).noargs();
    }
}
