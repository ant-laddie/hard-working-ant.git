package cn.xw.mq.consumer;

import cn.xw.config.RabbitMQConfig;
import cn.xw.entity.MessageSendDTO;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-16 15:04
 * 消费者信息
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class QueueConsumer {

    /***
     * 消费者1
     */
    @RabbitListener(queues = {RabbitMQConfig.Q1})
    public void testConsumerA(@Payload String msgData, //这个是生产者发送的JSON消息
                              Message message,
                              Channel channel) {
        log.info("接收到队列1信息；信息为：{}", JSONObject.parseObject(msgData, MessageSendDTO.class));

    }

    /***
     * 消费者2
     */
    @RabbitListener(queues = {RabbitMQConfig.Q2})
    public void testConsumerB(@Payload String msgData, //这个是生产者发送的JSON消息
                              Message message,
                              Channel channel) {
        log.info("接收到队列2信息......；信息为：{}", JSONObject.parseObject(msgData, MessageSendDTO.class));
    }
}
