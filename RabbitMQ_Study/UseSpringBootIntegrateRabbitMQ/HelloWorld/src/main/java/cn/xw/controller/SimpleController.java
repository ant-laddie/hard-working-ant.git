package cn.xw.controller;

import cn.xw.entity.MessageSendDTO;
import cn.xw.mq.producer.SimpleProducer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-13 23:52
 */
@Slf4j  //使用lombok自带的日志注解，具体实现是slf4j+log4j2
@RestController
@RequestMapping("/simple")
@RequiredArgsConstructor
public class SimpleController {

    //使用SLF4J来获取Logger对象;(注意导包：import org.slf4j.Logger; import org.slf4j.LoggerFactory;)
    //Logger logger = LoggerFactory.getLogger(this.getClass());

    //注入生产者对象
    private final SimpleProducer simpleProducer;

    /***
     * 基本的get请求，用来接收消息，并把消息交给生产者，并由生产者推送到指定交换机，由交换机分发消息
     * @param msg 请求消息
     * @return String
     */
    @PostMapping("/produce")
    public String produce(@RequestBody MessageSendDTO msg) {
        log.info("Controller接收到请求并把请求的信息交由生产者：{}", msg);
        //发送消息到生产者
        simpleProducer.productionSimpleMessage(msg);
        return "请求发送成功，并已接收";
    }
}
