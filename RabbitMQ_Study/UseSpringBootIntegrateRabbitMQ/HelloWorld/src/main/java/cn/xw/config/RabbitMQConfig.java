package cn.xw.config;


import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-13 17:55
 * RabbitMQ配置类
 */
@Configuration
public class RabbitMQConfig {

    //定义1：简单的直接交换机名称；定义2：简单队列名称；定义3：路由key
    public static final String SIMPLE_DIRECT_EXCHANGE = "simpleDirectExchange";
    public static final String SIMPLE_QUEUE_NAME = "simpleQueueName";
    public static final String SIMPLE_KEY = "simpleKey";

    /***
     * 创建交换机信息
     * @return Exchange
     */
    @Bean("simpleDirectExchange")   //注：Bean对象可以不写名称，默认就是方法名
    public Exchange simpleDirectExchange() {
        //这个ExchangeBuilder就是我们当初使用的如下方式一样：
        // channel.exchangeDeclare("交换机名称", "交换机类型",true, false, false, null);
        return ExchangeBuilder.directExchange(SIMPLE_DIRECT_EXCHANGE).durable(true).build();
    }

    /***
     * 创建队列信息
     * @return Queue
     */
    @Bean("simpleQueueName")
    public Queue simpleQueueName() {
        //这个QueueBuilder就是我们当初使用的如下方式一样：
        // channel.queueDeclare("队列名称", true, false, false, null);
        return QueueBuilder.durable(SIMPLE_QUEUE_NAME).build();
    }

    /***
     * 队列信息绑定到交换机上
     * @param simpleDirectExchange 简单的直接交换机
     * @param simpleQueueName 简单的队列
     * @return Binding
     */
    @Bean("simpleQueueBindSimpleExchange")
    public Binding simpleQueueBindSimpleExchange(@Qualifier(value = "simpleDirectExchange") Exchange simpleDirectExchange,
                                                 @Qualifier(value = "simpleQueueName") Queue simpleQueueName) {
        //这个BindingBuilder就是我们当初使用的如下方式一样：
        // channel.queueBind("队列名称", "交换机名称", "路由key");
        return BindingBuilder.bind(simpleQueueName).to(simpleDirectExchange).with(SIMPLE_KEY).noargs();
    }
}
