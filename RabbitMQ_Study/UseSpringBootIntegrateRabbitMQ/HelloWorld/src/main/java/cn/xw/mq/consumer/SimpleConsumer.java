package cn.xw.mq.consumer;

import cn.xw.config.RabbitMQConfig;
import cn.xw.entity.MessageSendDTO;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-13 23:29
 * 这是一个简单的消费者
 */
@Slf4j
@Component
public class SimpleConsumer {

    /***
     * 简单消息处理（监听）
     * @param msgData 传递的具体消息，最好是生产者发送使用什么类型，这里接收就用什么类型
     * @param message 这个就类似我们原生的message
     * @param channel 这个就类似我们原生的channel
     */
    @RabbitListener(queues = {RabbitMQConfig.SIMPLE_QUEUE_NAME}) //只需要监听队列即可,多个则在{}里面逗号分割
    public void messageSimpleHandle(String msgData, Message message, Channel channel) {
        //获取到队列消息，因为发送是JSON格式，我们要解析对象格式
        MessageSendDTO msg = JSONObject.parseObject(message.getBody(), MessageSendDTO.class);
        log.info("消息由消费者消费：{}，并消费完成", msg);
    }
}
