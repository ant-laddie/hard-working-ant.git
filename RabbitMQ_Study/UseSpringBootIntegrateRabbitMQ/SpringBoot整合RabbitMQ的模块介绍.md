### 1：HelloWorld 【入门案例】
    入门案例
### 2：WorkQueuesAck 【直接交换机+工作队列+消息应答+消息分发】
    一个生产者发送到具体的队列，由多个消费者来轮询处理任务消息
### 3：FanoutExchange 【扇出交换机 一个消息群发全部队列】
### 4：TopicssExchange 【主题交换机 一个消息可以按照指定的匹配模式发送到队列】
### 5：DLXQueueDemo 【死信队列】
### 6：DelayQueueDemo 【延迟队列，基于死信队列的，有瑕疵】
### 7：DelayExchangeDemoPlugins 【延迟消息，基于插件】
### 8：ReleaseConfirmationExchange 【发布确认模式】
### 9:BackupExchangeDemo 【备份交换机】
### 10：IdempotentConsumer 【RabbitMQ幂等性问题（重复消费）】
### 11：PriorityQueue 【延迟队列】
