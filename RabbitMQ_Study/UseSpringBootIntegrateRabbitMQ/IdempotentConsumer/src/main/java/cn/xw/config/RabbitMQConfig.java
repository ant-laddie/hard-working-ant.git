package cn.xw.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-20 16:56
 * RabbitMQ配置
 */
@Configuration
public class RabbitMQConfig {

    //普通直接交换机
    public static final String ORDINARY_DIRECT_EXCHANGE = "ordinaryDirectExchange";
    //普通队列
    public static final String ORDINARY_QUEUE = "ordinaryQueue";
    //路由key
    public static final String ROUTE_KEY = "routeKey";

    /***
     * 创建直接交换机
     * @return Exchange
     */
    @Bean("ordinaryDirectExchange")
    public Exchange ordinaryDirectExchange() {
        return ExchangeBuilder.directExchange(ORDINARY_DIRECT_EXCHANGE).durable(true).build();
    }

    /***
     * 创建普通队列
     * @return Queue
     */
    @Bean("ordinaryQueue")
    public Queue ordinaryQueue() {
        return QueueBuilder.durable(ORDINARY_QUEUE).build();
    }

    /***
     * 绑定关系
     * @param exchange 交换机
     * @param queue 队列
     * @return Binding
     */
    @Bean("ordinaryQueueBindOrdinaryDirectExchange")
    public Binding ordinaryQueueBindExchange(@Qualifier("ordinaryDirectExchange") Exchange exchange,
                                             @Qualifier("ordinaryQueue") Queue queue) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTE_KEY).noargs();
    }
}
