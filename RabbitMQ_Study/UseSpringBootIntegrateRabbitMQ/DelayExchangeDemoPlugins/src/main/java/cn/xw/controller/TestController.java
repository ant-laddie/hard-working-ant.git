package cn.xw.controller;

import cn.xw.entity.MessageSendDTO;
import cn.xw.mq.producer.TestProducer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-14 21:37
 */
@Slf4j
@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {

    //注入生产者对象
    private final TestProducer testProducer;

    /***
     * 基本的POST请求，用来接收消息，并把消息交给生产者，并由生产者推送到指定交换机，由交换机分发消息
     * @param msg 请求消息
     * @param delayTime 延迟时间
     * @return String
     */
    @PostMapping("/produceMsg/{delayTime}")
    public String msgSendSimple(@RequestBody MessageSendDTO msg, @PathVariable(value = "delayTime") Integer delayTime) {
        log.info("Controller接收到请求并把请求的信息交由生产者：{}，其中消息的过期时间为：{} s", msg, delayTime);
        //发送消息
        testProducer.producerSendMsgDelay(msg, delayTime);
        return "请求发送成功，并已接收";
    }
}
