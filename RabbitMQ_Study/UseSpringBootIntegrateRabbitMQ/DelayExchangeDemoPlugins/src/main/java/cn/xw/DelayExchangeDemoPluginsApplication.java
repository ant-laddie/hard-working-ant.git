package cn.xw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DelayExchangeDemoPluginsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DelayExchangeDemoPluginsApplication.class, args);
    }

}
