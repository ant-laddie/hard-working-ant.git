package cn.xw.mq.producer;

import cn.xw.config.RabbitMQConfig;
import cn.xw.entity.MessageSendDTO;
import com.alibaba.fastjson.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-16 15:01
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class TestProducer {

    private final RabbitTemplate rabbitTemplate;

    /***
     * 生产者方法
     * @param msg 消息
     * @param delayTime 延迟时间
     */
    public void producerSendMsgDelay(MessageSendDTO msg, Integer delayTime) {
        //消息转换为JSON格式并转为字节数组
        byte[] bytes = JSONObject.toJSONString(msg).getBytes(StandardCharsets.UTF_8);
        //发送消息
        rabbitTemplate.convertAndSend(RabbitMQConfig.DELAYED_EXCHANGE, RabbitMQConfig.DELAYED_ROUTING_KEY, bytes, message -> {
            //这条消息的过期时间被设置过期delayTime秒（在交换机中被延迟，时间一到则被路由到队列）
            message.getMessageProperties().setDelay(delayTime * 1000);
            //设置好了一定要返回
            return message;
        });
    }
}
