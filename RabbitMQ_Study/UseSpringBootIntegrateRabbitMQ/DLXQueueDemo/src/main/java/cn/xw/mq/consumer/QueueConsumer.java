package cn.xw.mq.consumer;

import cn.xw.config.RabbitMQConfig;
import cn.xw.entity.MessageSendDTO;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-16 15:04
 * 消费者信息
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class QueueConsumer {

    /***
     * 消费者
     * @param msgData 具体的消息
     * @param deliveryTag 处理消息的编号
     * @param routingKey 当前的路由key
     * @param message message对象
     * @param channel 信道对象
     */
    @RabbitListener(queues = {RabbitMQConfig.QUEUE_NAME}, ackMode = "MANUAL")  // MANUAL必须大写
    public void testConsumerA(@Payload String msgData, //这个是生产者发送的JSON消息
                              @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag, //处理消息的编号
                              @Header(AmqpHeaders.RECEIVED_ROUTING_KEY) String routingKey,
                              Message message,
                              Channel channel) throws IOException {
        //把接收JSON的数据转换为对象
        MessageSendDTO messageSendDTO = JSONObject.parseObject(msgData, MessageSendDTO.class);
        //模拟判断我是否需要手动确认(若随机不是2则确认消费，否则拒绝，交由死信队列)
        if (Math.ceil(Math.random() * 4) != 2) {
            log.info("处理完成接收到的队列信息为：{}，从{}路由过来的数据", messageSendDTO, routingKey);
            //手动确认
            channel.basicAck(deliveryTag, false);
        } else {
            log.info("未处理完成接收到的队列信息为：{}，从{}路由过来的数据", messageSendDTO, routingKey);
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
        }
    }
}
