package cn.xw.mq.consumer;


import cn.xw.config.RabbitMQConfig;
import cn.xw.entity.MessageSendDTO;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-13 23:29
 * 消费者
 */
@Slf4j
@Component
public class QueueConsumer {

    /***
     * 消费者（监听队列ordinaryQueue）
     * @param msgData 传递的具体消息，最好是生产者发送使用什么类型，这里接收就用什么类型
     * @param message 这个就类似我们原生的message
     * @param channel 这个就类似我们原生的channel
     */
    @RabbitListener(queues = {RabbitMQConfig.ORDINARY_QUEUE}, ackMode = "MANUAL")
    public void ordinaryQueueTest(@Payload String msgData, //这个是生产者发送的JSON消息
                                  @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag, //处理消息的编号
                                  Message message,
                                  Channel channel) throws IOException {
        //获取到队列消息，因为发送是JSON格式，我们要解析对象格式
        MessageSendDTO msg = JSONObject.parseObject(message.getBody(), MessageSendDTO.class);
        log.info("消息由消费者消费：{}，并消费完成", msg);
        //手动确认，注：这个deliveryTag可以通过message.getMessageProperties().getDeliveryTag()拿到
        channel.basicAck(deliveryTag, false);
    }
}
