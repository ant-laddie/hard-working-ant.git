package cn.xw.mq.consumer;

import cn.xw.config.RabbitMQConfig;
import cn.xw.entity.MessageSendDTO;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-16 15:04
 * 消费者信息
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class QueueConsumer {

    /***
     * 消费者A
     */
    @RabbitListener(queues = {RabbitMQConfig.QUEUE_A})
    public void testConsumerA(@Payload String msgData, //这个是生产者发送的JSON消息
                              Message message,
                              Channel channel) {
        log.info("接收到队列A信息......；信息为：{}", JSONObject.parseObject(msgData, MessageSendDTO.class));

    }

    /***
     * 消费者B
     */
    @RabbitListener(queues = {RabbitMQConfig.QUEUE_B})
    public void testConsumerB(@Payload String msgData, //这个是生产者发送的JSON消息
                              Message message,
                              Channel channel) {
        //注：若消费失败（报错）会自动手动不确认，并且把消息放到队列中，然后又被这个队列消费，最终死循环
        //int a = 1 / 0;
        log.info("接收到队列B信息......；信息为：{}", JSONObject.parseObject(msgData, MessageSendDTO.class));
    }
}
