package cn.xw.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-16 14:05
 * 扇出交换机配置
 */
@Configuration
public class RabbitMQConfig {

    //扇出交换机名称
    public static final String EXCHANGE_NAME = "fanoutDemo";
    //创建两个消息队列
    public static final String QUEUE_A = "queueA";
    public static final String QUEUE_B = "queueB";

    /***
     * 创建交换机信息
     * @return Exchange
     */
    @Bean("fanoutDemo")
    public Exchange fanoutDemo() {
        return ExchangeBuilder.fanoutExchange(EXCHANGE_NAME).durable(true).build();
    }

    /***
     * 创建队列A
     * @return Queue
     */
    @Bean("queueA")
    public Queue queueA() {
        return QueueBuilder.durable(QUEUE_A).build();
    }

    /***
     * 创建队列B
     * @return Queue
     */
    @Bean("queueB")
    public Queue queueB() {
        return QueueBuilder.durable(QUEUE_B).build();
    }

    /***
     * 队列A绑定到扇出交换机
     * @param fanoutDemo 交换机名称
     * @param queueA 队列A
     * @return Binding
     */
    @Bean("fanoutBindQueueA")
    public Binding fanoutBindQueueA(@Qualifier(value = "fanoutDemo") Exchange fanoutDemo,
                                    @Qualifier(value = "queueA") Queue queueA) {
        return BindingBuilder.bind(queueA).to(fanoutDemo).with("").noargs();
    }

    /***
     * 队列B绑定到扇出交换机
     * @param fanoutDemo 交换机名称
     * @param queueB 队列B
     * @return Binding
     */
    @Bean("fanoutBindQueueB")
    public Binding fanoutBindQueueB(@Qualifier(value = "fanoutDemo") Exchange fanoutDemo,
                                    @Qualifier(value = "queueB") Queue queueB) {
        return BindingBuilder.bind(queueB).to(fanoutDemo).with("").noargs();
    }
}
