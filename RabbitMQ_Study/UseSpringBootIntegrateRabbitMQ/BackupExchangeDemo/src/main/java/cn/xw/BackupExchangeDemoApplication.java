package cn.xw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackupExchangeDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackupExchangeDemoApplication.class, args);
    }

}
