package cn.xw.mq.consumer;

import cn.xw.config.RabbitMQConfig;
import cn.xw.entity.MessageSendDTO;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-19 0:56
 * 报警消费者
 */
@Slf4j
@Component
public class WarningConsumer {
    /***
     * 报警队列消费者接收的消息监听
     */
    @RabbitListener(queues = {RabbitMQConfig.WARNING_QUEUE}, ackMode = "MANUAL")
    public void warningConsumerTest(@Payload String msgData, Message message, Channel channel) throws IOException {
        //把接收过来的JSON信息转换为对象
        MessageSendDTO messageSendDTO = JSONObject.parseObject(msgData, MessageSendDTO.class);
        log.info("报警队列：{}，监听发送过来的数据并处理：{}",
                message.getMessageProperties().getConsumerQueue(), messageSendDTO);
        //手动确认
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
