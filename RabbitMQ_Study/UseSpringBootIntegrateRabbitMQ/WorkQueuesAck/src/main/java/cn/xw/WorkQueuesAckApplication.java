package cn.xw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkQueuesAckApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkQueuesAckApplication.class, args);
    }
}
/***
 发送的请求：localhost:8081/test/produce
    {
        "msgID": 1001,
        "msgType": "simpleMessage",
        "msgBody": "测试操作"
    }
 测试执行的结果：
 00:24:27.740 .. Controller接收到请求并把请求的信息交由生产者：MessageSendDTO(msgID=1001, msgType=simpleMessage, msgBody=测试操作)
 00:24:29.993 .. B：消息由消费者B消费：MessageSendDTO(msgID=98, msgType=testType, msgBody=测试操作bb)，并消费完成
 00:24:32.131 .. B：消息由消费者B消费：MessageSendDTO(msgID=99, msgType=testType, msgBody=测试操作cc)，并消费完成
 00:24:34.161 .. B：消息由消费者B消费：MessageSendDTO(msgID=100, msgType=testType, msgBody=测试操作dd)，并消费完成
 00:24:35.999 .. A：消息由消费者A消费：MessageSendDTO(msgID=97, msgType=testType, msgBody=测试操作aa)，并消费完成
 00:24:36.182 .. B：消息由消费者B消费：MessageSendDTO(msgID=101, msgType=testType, msgBody=测试操作ee)，并消费完成
 00:24:38.206 .. B：消息由消费者B消费：MessageSendDTO(msgID=103, msgType=testType, msgBody=测试操作gg)，并消费失败，丢回队列
 00:24:40.230 .. B：消息由消费者B消费：MessageSendDTO(msgID=103, msgType=testType, msgBody=测试操作gg)，并消费完成
 00:24:42.356 .. B：消息由消费者B消费：MessageSendDTO(msgID=104, msgType=testType, msgBody=测试操作hh)，并消费失败，丢回队列
 00:24:44.025 .. A：消息由消费者A消费：MessageSendDTO(msgID=102, msgType=testType, msgBody=测试操作ff)，并消费完成
 00:24:44.381 .. B：消息由消费者B消费：MessageSendDTO(msgID=104, msgType=testType, msgBody=测试操作hh)，并消费完成
 00:24:46.406 .. B：消息由消费者B消费：MessageSendDTO(msgID=106, msgType=testType, msgBody=测试操作jj)，并消费完成
 00:24:52.041 .. A：消息由消费者A消费：MessageSendDTO(msgID=105, msgType=testType, msgBody=测试操作ii)，并消费完成
 */
