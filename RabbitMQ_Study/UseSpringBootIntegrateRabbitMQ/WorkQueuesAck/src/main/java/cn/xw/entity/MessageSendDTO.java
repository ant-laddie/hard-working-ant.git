package cn.xw.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-14 11:49
 * 用来测试RabbitMQ的生产者发送消息（对象）到消费者中的一系列传输
 */
@Data
@Builder
public class MessageSendDTO implements Serializable {

    private static final long serialVersionUID = 5905249092659173678L;

    private Integer msgID;          // 消息ID
    private String msgType;         // 消息类型
    private Object msgBody;         // 消息体
}
