package cn.xw.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-14 17:43
 * RabbitMQ配置类
 */
@Configuration
public class RabbitMQConfig {
    //直接交换机名称
    public static final String DIRECT_EXCHANGE = "directExchange";
    //队列名称A
    public static final String QUEUE_A_NAME = "queueAName";
    //路由key
    public static final String ROUTE_KEY = "routeKeyName";

    /***
     * 创建交换机信息
     * @return Exchange
     */
    @Bean("directExchange")
    public Exchange directExchange() {
        return ExchangeBuilder.directExchange(DIRECT_EXCHANGE).durable(true).build();
    }

    /***
     * 创建队列A信息
     * @return Queue
     */
    @Bean("queueAName")
    public Queue queueAName() {
        return QueueBuilder.durable(QUEUE_A_NAME).build();
    }

    /***
     * 队列绑定到交换机上，通过路由key
     * @param directExchange 交换机信息
     * @param queueAName A队列绑定
     * @return Binding
     */
    @Bean("directExchangeBindAQueue")
    public Binding directExchangeBindAQueue(@Qualifier(value = "directExchange") Exchange directExchange,
                                            @Qualifier(value = "queueAName") Queue queueAName) {
        return BindingBuilder.bind(queueAName).to(directExchange).with(ROUTE_KEY).noargs();
    }
}
