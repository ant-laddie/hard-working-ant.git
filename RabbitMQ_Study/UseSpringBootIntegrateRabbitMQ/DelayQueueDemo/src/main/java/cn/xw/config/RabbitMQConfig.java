package cn.xw.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-17 17:10
 * RabbitMQ配置类
 */
@Configuration
public class RabbitMQConfig {

    //延迟直接交换机
    public static final String DELAY_DIRECT_EXCHANGE = "delayDirectExchange";
    //延迟队列
    public static final String DELAY_TTL_QUEUE = "delayTTLQueue";
    //延迟队列连接延迟交换机路由key
    public static final String DELAY_ROUTING_KEY = "delayRoutingKey";
    //死信交换机
    public static final String DEAD_EXCHANGE = "deadExchange";
    //死信队列
    public static final String DEAD_QUEUE = "deadQueue";
    //死信队列绑定死信交换机路由key
    public static final String DEAD_ROUTING_KEY = "deadRoutingKey";

    //===================================== 优化代码 Start
    //简单队列，无延迟
    public static final String SIMPLE_QUEUE = "simpleQueue";
    //简单队列连接延迟交换机路由key
    public static final String SIMPLE_ROUTING_KEY = "simpleRoutingKey";

    /***
     * 一个简单无延迟的队列
     * @return Queue
     */
    @Bean("simpleQueue")
    public Queue simpleQueue() {
        //绑定死信队列（参数设置）
        Map<String, Object> arguments = new HashMap<>();
        //正常队列设置死信交换机 参数key是固定值；（就是说死去的消息发送到哪个交换机）
        arguments.put("x-dead-letter-exchange", DEAD_EXCHANGE);
        //正常队列设置死信交换机到死信队列绑定Routing Key 参数key是固定值（就是说死去的消息在交换机里通过什么路由发送到死信队列）
        arguments.put("x-dead-letter-routing-key", DEAD_ROUTING_KEY);
        //设置正常队列的长度限制 为3
        //arguments.put("x-max-length",3);
        //队列设置消息过期时间 20 秒
        //arguments.put("x-message-ttl", 20 * 1000);
        return QueueBuilder.durable(SIMPLE_QUEUE).withArguments(arguments).build();
    }

    /***
     * 简单队列绑定到延迟交换机上
     * @param delayDirectExchange 延迟交换机
     * @param simpleQueue 简单队列
     * @return Binding
     */
    @Bean("simpleQueueBindDelayExchange")
    public Binding simpleQueueBindDelayExchange(@Qualifier("delayDirectExchange") Exchange delayDirectExchange,
                                                @Qualifier("simpleQueue") Queue simpleQueue) {
        return BindingBuilder.bind(simpleQueue).to(delayDirectExchange).with(SIMPLE_ROUTING_KEY).noargs();
    }
    //===================================== 优化代码 End

    //编写延迟队列和延迟交换机一些列配置

    /***
     * 延迟交换机
     * @return Exchange
     */
    @Bean("delayDirectExchange")
    public Exchange delayDirectExchange() {
        return ExchangeBuilder.directExchange(DELAY_DIRECT_EXCHANGE).durable(true).build();
    }

    /***
     * 延迟普通队列
     * @return Queue
     */
    @Bean("delayTTLQueue")
    public Queue delayTTLQueue() {
        //绑定死信队列（参数设置）
        Map<String, Object> arguments = new HashMap<>();
        //正常队列设置死信交换机 参数key是固定值；（就是说死去的消息发送到哪个交换机）
        arguments.put("x-dead-letter-exchange", DEAD_EXCHANGE);
        //正常队列设置死信交换机到死信队列绑定Routing Key 参数key是固定值（就是说死去的消息在交换机里通过什么路由发送到死信队列）
        arguments.put("x-dead-letter-routing-key", DEAD_ROUTING_KEY);
        //设置正常队列的长度限制 为3
        //arguments.put("x-max-length",3);
        //队列设置消息过期时间 20 秒
        arguments.put("x-message-ttl", 20 * 1000);
        return QueueBuilder.durable(DELAY_TTL_QUEUE).withArguments(arguments).build();
    }

    /***
     * 延迟队列绑定到延迟交换机上
     * @param delayDirectExchange 延迟交换机
     * @param delayTTLQueue 延迟队列
     * @return Binding
     */
    @Bean("delayQueueBindDelayExchange")
    public Binding delayQueueBindDelayExchange(@Qualifier("delayDirectExchange") Exchange delayDirectExchange,
                                               @Qualifier("delayTTLQueue") Queue delayTTLQueue) {
        return BindingBuilder.bind(delayTTLQueue).to(delayDirectExchange).with(DELAY_ROUTING_KEY).noargs();

    }

    //编写死信队列和死信交换机，和它们绑定关系的配置

    /***
     * 死信交换机
     * @return Exchange
     */
    @Bean("deadExchange")
    public Exchange deadExchange() {
        return ExchangeBuilder.directExchange(DEAD_EXCHANGE).durable(true).build();
    }

    /***
     * 死信队列
     * @return Queue
     */
    @Bean("deadQueue")
    public Queue deadQueue() {
        return QueueBuilder.durable(DEAD_QUEUE).build();
    }

    /***
     * 死信队列绑定死信交换机
     * @param deadExchange 死信交换机
     * @param deadQueue 死信队列
     * @return Binding
     */
    @Bean("deadQueueBindDeadExchange")
    public Binding deadQueueBindDeadExchange(@Qualifier("deadExchange") Exchange deadExchange,
                                             @Qualifier("deadQueue") Queue deadQueue) {
        return BindingBuilder.bind(deadQueue).to(deadExchange).with(DEAD_ROUTING_KEY).noargs();
    }
}
