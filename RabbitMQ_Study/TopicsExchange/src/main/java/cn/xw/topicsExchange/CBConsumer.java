package cn.xw.topicsExchange;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-13 11:13
 * 消费者B
 */
public class CBConsumer {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //交换机名称
    public static final String TOPIC_EXCHANGE = "TopicExchange";
    //队列Q2名称
    public static final String Q2 = "Q2Queue";
    //路由绑定关系 Routing Key 1
    public static final String Q2_KEY_A = "*.*.rabbit";
    //路由绑定关系 Routing Key 2
    public static final String Q2_KEY_B = "lazy.#";

    public static void main(String[] args) throws IOException {

        //调用自己的工具类获取信道
        Channel channel = ChannelUtil.getChannel();

        //声明exchange交换机 并设置为主题交换机；防止消费者先启动报错，找不到交换机
        //channel.exchangeDeclare(TOPIC_EXCHANGE, BuiltinExchangeType.TOPIC,
        //        true, false, false, null);

        //创建一个基本日志队列
        channel.queueDeclare(Q2, true, false, false, null);
        //队列绑定到交换机上，并通过路由key来对应两者的连接(这里设置了2个路由连接)
        channel.queueBind(Q2, TOPIC_EXCHANGE, Q2_KEY_A);
        channel.queueBind(Q2, TOPIC_EXCHANGE, Q2_KEY_B);

        logger.info("CAConsumer(消费者B)开始监听Q2队列消息....");
        //接收队列消息
        channel.basicConsume(Q2, false, (consumerTag, message) -> {
            //当前接收的消息
            String msg = new String(message.getBody(), StandardCharsets.UTF_8);
            //当前从哪个路由key上获取的消息
            String routingKey = message.getEnvelope().getRoutingKey();
            //当前从哪个交换机里分配的消息
            String exchange = message.getEnvelope().getExchange();
            logger.info("CBConsumer(消费者B)获取队列信息并处理：{}，绑定的路由名称：{}，" +
                    "分发过来的路由名称：{}，哪个交换机分发过来的消息：{}", msg, Q2_KEY_A+" AND "+Q2_KEY_B, routingKey, exchange);
            //手动确认
            channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
        }, consumerTag -> {
            logger.info("CBConsumer(消费者B)出现异常；可能队列被删除！{}", consumerTag);
        });
    }
}
