package cn.xw.topicsExchange;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-13 11:07
 * 消费者A
 */
public class CAConsumer {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //交换机名称
    public static final String TOPIC_EXCHANGE = "TopicExchange";
    //队列Q1名称
    public static final String Q1 = "Q1Queue";
    //路由绑定关系 Routing Key
    public static final String Q1_KEY = "*.orange.*";

    public static void main(String[] args) throws IOException {

        //调用自己的工具类获取信道
        Channel channel = ChannelUtil.getChannel();

        //声明exchange交换机 并设置为主题交换机；防止消费者先启动报错，找不到交换机
        //channel.exchangeDeclare(TOPIC_EXCHANGE, BuiltinExchangeType.TOPIC,
        //        true, false, false, null);

        //创建一个基本日志队列
        channel.queueDeclare(Q1, true, false, false, null);
        //队列绑定到交换机上，并通过路由key来对应两者的连接
        channel.queueBind(Q1, TOPIC_EXCHANGE, Q1_KEY);

        logger.info("CAConsumer(消费者A)开始监听Q1队列消息....");
        //接收队列消息
        channel.basicConsume(Q1, false, (consumerTag, message) -> {
            //当前接收的消息
            String msg = new String(message.getBody(), StandardCharsets.UTF_8);
            //当前从哪个路由key上获取的消息
            String routingKey = message.getEnvelope().getRoutingKey();
            //当前从哪个交换机里分配的消息
            String exchange = message.getEnvelope().getExchange();
            logger.info("CAConsumer(消费者A)获取队列信息并处理：{}，绑定的路由名称：{}，" +
                    "分发过来的路由名称：{}，哪个交换机分发过来的消息：{}", msg, Q1_KEY, routingKey, exchange);
            //手动确认
            channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
        }, consumerTag -> {
            logger.info("CAConsumer(消费者A)出现异常；可能队列被删除！{}", consumerTag);
        });
    }
}
