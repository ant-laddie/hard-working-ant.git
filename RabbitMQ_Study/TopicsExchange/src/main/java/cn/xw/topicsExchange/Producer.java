package cn.xw.topicsExchange;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-11 23:56
 * 生产者（生产任务往队列推送）
 */
public class Producer {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //交换机名称
    public static final String TOPIC_EXCHANGE = "TopicExchange";

    public static void main(String[] args) throws IOException {
        //获取信道
        Channel channel = ChannelUtil.getChannel();

        //声明exchange交换机 并设置为主题交换机；
        channel.exchangeDeclare(TOPIC_EXCHANGE, BuiltinExchangeType.TOPIC,
                true, false, false, null);

        //消息任务准备
        HashMap<String, String> sendMsg = new HashMap<>();
        sendMsg.put("quick.orange.rabbit", "被队列 Q1 Q2 接收到");
        sendMsg.put("lazy.orange.elephant", "被队列 Q1 Q2 接收到");
        sendMsg.put("quick.orange.fox", "被队列 Q1 接收到");
        sendMsg.put("lazy.brown.fox", "被队列 Q2 接收到");
        sendMsg.put("lazy.pink.rabbit", "虽然满足两个绑定规则但两个规则都是在Q2队列，所有只要Q2接收一次");
        sendMsg.put("quick.brown.fox", "不匹配任何绑定不会被任何队列接收到会被丢弃");
        sendMsg.put("quick.orange.male.rabbit", "是四个单词不匹配任何绑定会被丢弃");
        sendMsg.put("lazy.orange.male.rabbit", "是四个单词但匹配 Q2");
        //循环发送消息任务
        for (Map.Entry<String, String> msg : sendMsg.entrySet()) {
            String routKey = msg.getKey();  //主题路由key
            String message = msg.getValue();//消息任务
            channel.basicPublish(TOPIC_EXCHANGE, routKey, MessageProperties.PERSISTENT_TEXT_PLAIN,
                    message.getBytes(StandardCharsets.UTF_8));
        }
        logger.info("生产者消息发送完成...");
    }
}

/***
 11:36:40.369 [main] INFO  cn.xw.topicsExchange.Producer - 生产者消息发送完成...
 11:34:19.339 [main] INFO  cn.xw.topicsExchange.Producer - CAConsumer(消费者A)开始监听Q1队列消息....
 11:36:19.227 [main] INFO  cn.xw.topicsExchange.Producer - CAConsumer(消费者B)开始监听Q2队列消息....
 11:36:40.369 [main] INFO  cn.xw.topicsExchange.Producer - 生产者消息发送完成...
 11:36:40.938 [pool-2-thread-4] INFO  cn.xw.topicsExchange.Producer - CBConsumer(消费者B)获取队列信息并处理：被队列 Q1 Q2 接收到，绑定的路由名称：*.*.rabbit AND lazy.#，分发过来的路由名称：lazy.orange.elephant，哪个交换机分发过来的消息：TopicExchange
 11:36:40.939 [pool-2-thread-4] INFO  cn.xw.topicsExchange.Producer - CBConsumer(消费者B)获取队列信息并处理：被队列 Q2 接收到，绑定的路由名称：*.*.rabbit AND lazy.#，分发过来的路由名称：lazy.brown.fox，哪个交换机分发过来的消息：TopicExchange
 11:36:40.939 [pool-2-thread-4] INFO  cn.xw.topicsExchange.Producer - CBConsumer(消费者B)获取队列信息并处理：被队列 Q1 Q2 接收到，绑定的路由名称：*.*.rabbit AND lazy.#，分发过来的路由名称：quick.orange.rabbit，哪个交换机分发过来的消息：TopicExchange
 11:36:40.940 [pool-2-thread-5] INFO  cn.xw.topicsExchange.Producer - CBConsumer(消费者B)获取队列信息并处理：虽然满足两个绑定规则但两个规则都是在Q2队列，所有只要Q2接收一次，绑定的路由名称：*.*.rabbit AND lazy.#，分发过来的路由名称：lazy.pink.rabbit，哪个交换机分发过来的消息：TopicExchange
 11:36:40.941 [pool-2-thread-5] INFO  cn.xw.topicsExchange.Producer - CBConsumer(消费者B)获取队列信息并处理：是四个单词但匹配 Q2，绑定的路由名称：*.*.rabbit AND lazy.#，分发过来的路由名称：lazy.orange.male.rabbit，哪个交换机分发过来的消息：TopicExchange
 11:36:40.961 [pool-2-thread-4] INFO  cn.xw.topicsExchange.Producer - CAConsumer(消费者A)获取队列信息并处理：被队列 Q1 Q2 接收到，绑定的路由名称：*.orange.*，分发过来的路由名称：lazy.orange.elephant，哪个交换机分发过来的消息：TopicExchange
 11:36:40.963 [pool-2-thread-4] INFO  cn.xw.topicsExchange.Producer - CAConsumer(消费者A)获取队列信息并处理：被队列 Q1 Q2 接收到，绑定的路由名称：*.orange.*，分发过来的路由名称：quick.orange.rabbit，哪个交换机分发过来的消息：TopicExchange
 11:36:40.963 [pool-2-thread-4] INFO  cn.xw.topicsExchange.Producer - CAConsumer(消费者A)获取队列信息并处理：被队列 Q1 接收到，绑定的路由名称：*.orange.*，分发过来的路由名称：quick.orange.fox，哪个交换机分发过来的消息：TopicExchange
 */
