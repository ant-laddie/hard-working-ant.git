package cn.xw.workQueues;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-12 0:09
 * 消费者A
 */
public class ConsumerA {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //简单队列名称
    public static final String QUEUE_NAME = "WorkQueuesDemo";

    public static void main(String[] args) throws IOException {
        //调用自己的工具类获取信道
        Channel channel = ChannelUtil.getChannel();
        //创建队列 以防启动消费者发现队列不存在报错
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        logger.info("消费者A开始监听队列消息....");
        //消费者消费消息
        channel.basicConsume(QUEUE_NAME, true, (consumerTag, message) -> {
            logger.info("A消费者获取队列信息并处理：{}", new String(message.getBody(), StandardCharsets.UTF_8));
        }, consumerTag -> {
            logger.info("监听的队列出现异常；可能队列被删除！{}", consumerTag);
        });
    }
}
