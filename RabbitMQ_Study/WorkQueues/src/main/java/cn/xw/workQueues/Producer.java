package cn.xw.workQueues;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-11 23:56
 * 生产者（生产任务往队列推送）
 */
public class Producer {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //简单队列名称
    public static final String QUEUE_NAME = "WorkQueuesDemo";

    public static void main(String[] args) throws IOException {
        //获取信道
        Channel channel = ChannelUtil.getChannel();
        //创建一个队列消息
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        //循环发送消息到队列中
        for (int i = 0; i < 10; i++) {
            byte[] msg = ("这是一个编号为：" + i + " 的待处理的消息").getBytes(StandardCharsets.UTF_8);
            channel.basicPublish("", QUEUE_NAME, null, msg);
        }
        logger.info("工作队列的生产者消息发送完成！");
    }
}
