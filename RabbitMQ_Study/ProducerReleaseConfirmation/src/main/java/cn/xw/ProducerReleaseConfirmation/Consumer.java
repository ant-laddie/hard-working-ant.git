package cn.xw.ProducerReleaseConfirmation;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-11 22:14
 * 消费者（消费任务）
 */
public class Consumer {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //【本demo是测试发布确认，所以是生产者和RabbitMQ打交到，我们无需消费，若需要消费则替换不同队列名称】
    //单个发布确认
    public static final String SINGLE_RELEASE_CONFIRMATION = "singleReleaseConfirmation";
    //批量确认
    public static final String BATCH_CONFIRMATION = "batchConfirmation";
    //异步发布确认
    public static final String ASYNC_RELEASE_CONFIRMATION = "asyncReleaseConfirmation";

    public static void main(String[] args) throws IOException {
        //通过工具类获取一个信道
        Channel channel = ChannelUtil.getChannel();
        //设置信道的预取值（可同时容纳的队列推送过来的消息）
        channel.basicQos(5);
        logger.info("消费者开始监听队列消息....");
        //消费者消费消息
        //参数一：消费哪个队列
        //参数二：消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
        //参数三：接受队列消息的回调接口
        //参数四：取消消费的回调接口
        channel.basicConsume(SINGLE_RELEASE_CONFIRMATION, true, (consumerTag, message) -> {
            logger.info("获取队列信息：{}", new String(message.getBody(), StandardCharsets.UTF_8));
        }, consumerTag -> {
            logger.info("监听的队列出现异常；可能队列被删除！{}", consumerTag);
        });
    }
}