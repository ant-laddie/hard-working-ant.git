package cn.xw.QueuesMessageDistribution;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-11 23:56
 * 生产者（生产任务往队列推送）
 */
public class Producer {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //简单队列名称（消息分发测试）
    public static final String QUEUE_NAME = "QueuesMessageDistribution";

    public static void main(String[] args) throws IOException {
        //获取信道
        Channel channel = ChannelUtil.getChannel();
        //创建一个队列消息
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);

        //第一种输入的发送方式
        //输入对象
//        Scanner scanner = new Scanner(System.in);
//        //循环发送消息到队列中
//        for (int i = 1; i <= 100; i++) {
//            System.out.println("输入发送的消息：");
//            byte[] msg = ("这是一个编号为：" + scanner.next() + " 的待处理的消息").getBytes(StandardCharsets.UTF_8);
//            channel.basicPublish("", QUEUE_NAME, null, msg);
//        }

        //第二种循环的发送方式
        //循环发送消息到队列中
        for (int i = 1; i <= 10; i++) {
            byte[] msg = ("这是一个编号为：" + i + " 的待处理的消息").getBytes(StandardCharsets.UTF_8);
            channel.basicPublish("", QUEUE_NAME, null, msg);
        }
        logger.info("工作队列的生产者消息发送完成！");
    }
}
/**
 * 说明：若预取值等于5时：channel.basicQos(5);   生产者发送10条消息则具体日志执行如下：
 * 14:34:32.439 [main] INFO  cn.xw.workQueues.Producer - 消费者A开始监听队列消息....
 * 14:34:39.855 [main] INFO  cn.xw.workQueues.Producer - 消费者B开始监听队列消息....
 * 14:34:50.040 [main] INFO  cn.xw.workQueues.Producer - 工作队列的生产者消息发送完成！
 * 14:34:51.096 [pool-2-thread-4] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：1 的待处理的消息
 * 14:34:52.106 [pool-2-thread-4] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：3 的待处理的消息
 * 14:34:53.111 [pool-2-thread-5] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：5 的待处理的消息
 * 14:34:54.118 [pool-2-thread-5] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：7 的待处理的消息
 * 14:34:55.122 [pool-2-thread-5] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：9 的待处理的消息
 * 14:35:05.094 [pool-2-thread-4] INFO  cn.xw.workQueues.Producer - B消费者获取队列信息并用时15秒完成处理：这是一个编号为：2 的待处理的消息
 * 14:35:20.110 [pool-2-thread-5] INFO  cn.xw.workQueues.Producer - B消费者获取队列信息并用时15秒完成处理：这是一个编号为：4 的待处理的消息
 * 14:35:35.113 [pool-2-thread-5] INFO  cn.xw.workQueues.Producer - B消费者获取队列信息并用时15秒完成处理：这是一个编号为：6 的待处理的消息
 * 14:35:50.123 [pool-2-thread-5] INFO  cn.xw.workQueues.Producer - B消费者获取队列信息并用时15秒完成处理：这是一个编号为：8 的待处理的消息
 * 14:36:05.135 [pool-2-thread-5] INFO  cn.xw.workQueues.Producer - B消费者获取队列信息并用时15秒完成处理：这是一个编号为：10 的待处理的消息
 * <p>
 * 说明：若预取值等于1时（就代表了不公平分发）：channel.basicQos(1);   生产者发送10条消息则具体日志执行如下：
 * 14:38:28.723 [main] INFO  cn.xw.workQueues.Producer - 消费者A开始监听队列消息....
 * 14:38:43.509 [main] INFO  cn.xw.workQueues.Producer - 消费者B开始监听队列消息....
 * 14:39:55.512 [main] INFO  cn.xw.workQueues.Producer - 工作队列的生产者消息发送完成！
 * 14:39:56.561 [pool-2-thread-4] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：1 的待处理的消息
 * 14:39:57.586 [pool-2-thread-5] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：3 的待处理的消息
 * 14:39:58.610 [pool-2-thread-6] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：4 的待处理的消息
 * 14:39:59.635 [pool-2-thread-7] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：5 的待处理的消息
 * 14:40:00.660 [pool-2-thread-8] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：6 的待处理的消息
 * 14:40:01.676 [pool-2-thread-9] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：7 的待处理的消息
 * 14:40:02.714 [pool-2-thread-10] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：8 的待处理的消息
 * 14:40:03.739 [pool-2-thread-3] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：9 的待处理的消息
 * 14:40:04.764 [pool-2-thread-4] INFO  cn.xw.workQueues.Producer - A消费者获取队列信息并用时1秒完成处理：这是一个编号为：10 的待处理的消息
 * 14:40:10.567 [pool-2-thread-4] INFO  cn.xw.workQueues.Producer - B消费者获取队列信息并用时15秒完成处理：这是一个编号为：2 的待处理的消息
 */
