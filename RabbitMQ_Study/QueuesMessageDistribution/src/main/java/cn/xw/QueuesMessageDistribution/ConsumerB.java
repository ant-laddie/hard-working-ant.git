package cn.xw.QueuesMessageDistribution;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-12 0:09
 * 消费者A
 */
public class ConsumerB {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //简单队列名称（消息分发测试）
    public static final String QUEUE_NAME = "QueuesMessageDistribution";

    public static void main(String[] args) throws IOException {
        //调用自己的工具类获取信道
        Channel channel = ChannelUtil.getChannel();
        //创建队列 以防启动消费者发现队列不存在报错
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        //设置分发规则（预取值）
        channel.basicQos(1); // 设置1可以代表不公平分发，执行完手动确认后，队列才会分配下一个消息处理
        //channel.basicQos(5); // 代表当前消费者A的信道堆积的未处理的消息数量
        logger.info("消费者B开始监听队列消息....");
        //消费者消费消息
        channel.basicConsume(QUEUE_NAME, false, (consumerTag, message) -> {
            //休眠
            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("B消费者获取队列信息并用时15秒完成处理：{}", new String(message.getBody(), StandardCharsets.UTF_8));
            //手动确认应答 不批量应答
            channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
        }, consumerTag -> {
            logger.info("监听的队列出现异常；可能队列被删除！{}", consumerTag);
        });
    }
}