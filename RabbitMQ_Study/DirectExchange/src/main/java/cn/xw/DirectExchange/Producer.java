package cn.xw.DirectExchange;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-11 23:56
 * 生产者（生产任务往队列推送）
 */
public class Producer {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //交换机名称
    public static final String LOG_EXCHANGE = "LogExchange";

    public static void main(String[] args) throws IOException {
        //获取信道
        Channel channel = ChannelUtil.getChannel();

        //声明exchange交换机 并设置为直接交换机；
        channel.exchangeDeclare(LOG_EXCHANGE, BuiltinExchangeType.DIRECT,
                true, false, false, null);

        //待发送的消息
        HashMap<String, List<String>> sendMsg = new HashMap<>();
        List<String> errMsg = Arrays.asList("[1001]系统存在重大问题，可能会发生宕机！！", "[1002]电脑受到蠕虫病毒攻击！！");
        List<String> basicMsg = Arrays.asList("[2001]尊敬的蚂蚁小哥欢迎登录系统", "[2002]蚂蚁小哥已退出账号");
        sendMsg.put("ErrKey", errMsg);
        sendMsg.put("BasicLogKey", basicMsg);
        //循环发送消息任务
        for (Map.Entry<String, List<String>> msg : sendMsg.entrySet()) {
            String key = msg.getKey();//路由key
            List<String> messages = msg.getValue();//待发送消息
            for (String message : messages) {
                channel.basicPublish(LOG_EXCHANGE, key, MessageProperties.PERSISTENT_TEXT_PLAIN,
                        message.getBytes(StandardCharsets.UTF_8));
            }
        }
        logger.info("生产者消息发送完成...");
    }
}
