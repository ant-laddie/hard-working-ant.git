package cn.xw.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-11 23:40
 * 创建信道的工具类
 */
public class ChannelUtil {
    //初始化消息
    private static final String HOST = "49.235.99.193";
    private static final String USER_NAME = "admin";
    private static final String PASSWORD = "123";
    private static final String VIRTUAL_HOST = "test";

    /***
     * 获取信道
     * @return Channel
     */
    public static Channel getChannel() {
        //信道初始化
        Channel channel = null;
        try {
            //创建一个连接工厂
            ConnectionFactory factory = new ConnectionFactory();
            //设置RabbitMQ服务的IP、账号、密码、Vhost虚拟主机(默认 "/" 则不需要设置)
            factory.setHost(HOST);
            factory.setUsername(USER_NAME);
            factory.setPassword(PASSWORD);
            factory.setVirtualHost(VIRTUAL_HOST);
            //通过工厂对象获取一个连接
            Connection connection = factory.newConnection();
            //通过连接来获取一个信道
            channel = connection.createChannel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return channel;
    }
}
