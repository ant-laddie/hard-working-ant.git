package cn.xw.fanoutExchange;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-11 22:14
 * 消费者A（消费任务）
 */
public class ConsumerA {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //扇出交换机名称
    public static final String EXCHANGE_NAME = "fanoutDemo";

    public static void main(String[] args) throws IOException {
        //通过工具类获取一个信道
        Channel channel = ChannelUtil.getChannel();

        //声明exchange交换机 并设置发布订阅模式（扇出模式）;防止消费者先启动报错，找不到交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT,
                true, false, false, null);

        //创建一个队列(第一种方式创建一个自定义队列；第二种方式创建一个随机队列)
        //channel.queueDeclare("fanoutQueue",true,false,false,null);
        String queueName = channel.queueDeclare().getQueue();// 这种方式是临时队列，不能持久化，并且结束后会自动删除
        //把队列绑定到指定交换机上
        channel.queueBind(queueName, EXCHANGE_NAME, "");
        logger.info("消费者A开始监听队列消息....");
        //接收消息
        channel.basicConsume(queueName, false, (consumerTag, message) -> {
            logger.info("A消费者获取队列信息正在处理：{}", new String(message.getBody(), StandardCharsets.UTF_8));
            //手动应答,不批量应答
            channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
        }, consumerTag -> {
            logger.info("监听的队列出现异常；可能队列被删除！{}", consumerTag);
        });
    }
}