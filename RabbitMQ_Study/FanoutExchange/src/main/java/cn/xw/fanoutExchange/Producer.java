package cn.xw.fanoutExchange;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-11 22:14
 * 生产者（生产任务）
 */
public class Producer {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //扇出交换机名称
    public static final String EXCHANGE_NAME = "fanoutDemo";

    public static void main(String[] args) throws IOException {
        //通过工具类获取一个信道
        Channel channel = ChannelUtil.getChannel();
        //声明exchange交换机 并设置发布订阅模式（扇出模式）
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT,
                true, false, false, null);
        //发送消息
        for (int i = 1; i <= 10; i++) {
            byte[] msg = ("这是一个编号为：" + i + " 的待处理的消息").getBytes(StandardCharsets.UTF_8);
            //发送消息到交换机 EXCHANGE_NAME
            channel.basicPublish(EXCHANGE_NAME, "", MessageProperties.PERSISTENT_TEXT_PLAIN, msg);
        }
        logger.info("生产者消息发送完成...");
    }
}
