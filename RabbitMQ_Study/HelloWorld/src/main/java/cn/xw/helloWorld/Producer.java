package cn.xw.helloWorld;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-11 22:14
 * 生产者（生产任务）
 */
public class Producer {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //简单队列名称
    public static final String QUEUE_NAME = "helloWorldQueue";

    public static void main(String[] args) throws IOException {

        //通过工具类获取一个信道
        Channel channel = ChannelUtil.getChannel();
        //声明一个队列
        //参数一：队列名称
        //参数二：队列里的消息是否持久化，默认消息保存在内存中，默认false
        //参数三：该队列是否只供一个消费者进行消费的独占队列，则为 true（仅限于此连接），false（默认，可以多个消费者消费）
        //参数四：是否自动删除 最后一个消费者断开连接以后 该队列是否自动删除 true 自动删除，默认false
        //参数五：构建队列的其它属性，看下面扩展参数
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        //发送的消息
        byte[] msg = "这是一个简单消息".getBytes(StandardCharsets.UTF_8);
        //发送消息
        //参数一：将发送到RabbitMQ的哪个交换机上
        //参数二：路由的key是什么（直接交换机找到路由后，通过路由key来确定最终的队列）
        //参数三：其它参数
        //参数四：发送到队列的具体信息
        channel.basicPublish("", QUEUE_NAME, null, msg);
        logger.info("生产者消息发送完成");
    }
}
