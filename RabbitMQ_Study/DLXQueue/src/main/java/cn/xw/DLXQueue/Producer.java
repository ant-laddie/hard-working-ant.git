package cn.xw.DLXQueue;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-11 22:14
 * 生产者（生产任务）
 */
public class Producer {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //直接交换机名称
    public static final String EXCHANGE_NAME = "MsgHandleExchange";
    //路由key
    public static final String ROUTING_KEY = "MsgHandleKey";

    public static void main(String[] args) throws IOException {
        //通过工具类获取一个信道
        Channel channel = ChannelUtil.getChannel();
        //声明exchange交换机 并设置直接交换机（路由模式）;防止消费者先启动报错，找不到交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT, true,
                false, false, null);

        //设置需要发送的内容
        List<String> msgContentList = Arrays.asList("aaa", "bbb", "ccc", "abb", "acc");
        //发送消息
        msgContentList.forEach(e -> {
            try {
                byte[] msg = ("[消息：" + e + "]").getBytes(StandardCharsets.UTF_8);
                //每隔2秒发送消息到交换机 EXCHANGE_NAME
                Thread.sleep(2000);
                //发送消息的参数设置 expiration过期时间10秒   deliveryMode 消息持久化方式
                AMQP.BasicProperties properties = new AMQP.BasicProperties()
                        .builder().expiration("10000").deliveryMode(2).build();
                channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY, properties, msg);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        logger.info("生产者消息发送完成...");
    }
}
/***
 测试结果：注先启动生产者->死信队列->消费者->然后生产者发送消息
 14:21:55.266 [main] INFO  cn.xw.DLXQueue.Producer - 生产者消息发送完成...
 14:21:56.260 [main] INFO  cn.xw.DLXQueue.Producer - 死信队列开始监听队列消息....
 14:22:02.077 [main] INFO  cn.xw.DLXQueue.Producer - 消费者开始监听队列消息....
 14:22:21.874 [pool-2-thread-4] INFO  cn.xw.DLXQueue.Producer - 消费者获取队列信息正在处理：[消息：aaa]
 14:22:21.875 [pool-2-thread-4] INFO  cn.xw.DLXQueue.Producer - =======》 √ 消息已被成功处理：[消息：aaa]
 14:22:23.842 [pool-2-thread-5] INFO  cn.xw.DLXQueue.Producer - 消费者获取队列信息正在处理：[消息：bbb]
 14:22:23.843 [pool-2-thread-5] INFO  cn.xw.DLXQueue.Producer - =======》 X 消息已被拒绝处理，并丢弃：[消息：bbb]
 14:22:23.891 [pool-2-thread-4] INFO  cn.xw.DLXQueue.Producer - 死信队列接收并处理本次消息：[消息：bbb]
 14:22:25.861 [pool-2-thread-6] INFO  cn.xw.DLXQueue.Producer - 消费者获取队列信息正在处理：[消息：ccc]
 14:22:25.862 [pool-2-thread-6] INFO  cn.xw.DLXQueue.Producer - =======》 √ 消息已被成功处理：[消息：ccc]
 14:22:27.846 [pool-2-thread-7] INFO  cn.xw.DLXQueue.Producer - 消费者获取队列信息正在处理：[消息：abb]
 14:22:27.847 [pool-2-thread-7] INFO  cn.xw.DLXQueue.Producer - =======》 X 消息已被拒绝处理，并丢弃：[消息：abb]
 14:22:27.867 [pool-2-thread-5] INFO  cn.xw.DLXQueue.Producer - 死信队列接收并处理本次消息：[消息：abb]
 14:22:29.850 [main] INFO  cn.xw.DLXQueue.Producer - 生产者消息发送完成...
 14:22:29.861 [pool-2-thread-8] INFO  cn.xw.DLXQueue.Producer - 消费者获取队列信息正在处理：[消息：acc]
 14:22:29.862 [pool-2-thread-8] INFO  cn.xw.DLXQueue.Producer - =======》 √ 消息已被成功处理：[消息：acc]
 */
