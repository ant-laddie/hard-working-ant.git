package cn.xw.workQueuesAck;

import cn.xw.utils.ChannelUtil;
import com.rabbitmq.client.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author AnHui OuYang
 * @version 1.0
 * created at 2023-04-12 0:09
 * 消费者A
 */
public class ConsumerB {

    //通过日志管理器获取Logger对象
    static Logger logger = LogManager.getLogger(Producer.class);
    //简单队列名称
    public static final String QUEUE_NAME = "WorkQueuesAckDemo";

    public static void main(String[] args) throws IOException {
        //调用自己的工具类获取信道
        Channel channel = ChannelUtil.getChannel();
        //创建队列 以防启动消费者发现队列不存在报错
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        logger.info("消费者B开始监听队列消息....");
        //设置手动应答
        boolean autoAck = false;
        //消费者消费消息
        channel.basicConsume(QUEUE_NAME, autoAck, (consumerTag, message) -> {
            logger.info("B消费者已经接收到队列发送的ID为：{} 的消息", message.getEnvelope().getDeliveryTag());
            logger.info("B消费者获取队列信息并成功处理：{}", new String(message.getBody(), StandardCharsets.UTF_8));
            //手动确认应答，不批量应答
            channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
            logger.info("B消费者已经成功应答，ID为：{} 的消息", message.getEnvelope().getDeliveryTag());
        }, consumerTag -> {
            logger.info("监听的队列出现异常；可能队列被删除！{}", consumerTag);
        });
    }
}
