package cn.xw.websocket.handler;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 老师处理器（这里可以理解是我们创建了一个Controller）
 * WebSocket消息处理，这里以TextWebSocketHandler文本的处理方式
 *
 * @author Anhui OuYang
 * @version 1.0
 **/
@Slf4j          // 日志记录
@Component      // 注册到容器
public class TeacherHandler extends TextWebSocketHandler {

    // 初始化定时器对象
    private ScheduledExecutorService executorService = null;

    //在这里我们就可以注入Service对象来处理，在Service调用Mapper
    //@Autowired
    //private TeacherServiceImpl teacherService;

    /***
     * 在WebSocket连接建立后调用
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.info("afterConnectionEstablished() TeacherHandler方法执行，在WebSocket连接建立后调用....");
        // 建立连接后需要初始化定时器对象
        executorService = Executors.newSingleThreadScheduledExecutor();
    }

    /***
     * 处理WebSocket文本消息（若处理二进制非文本数据则使用handleMessage()方法或者继承BinaryWebSocketHandler类）
     */
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        log.info("handleTextMessage() TeacherHandler方法执行，处理WebSocket文本消息....");
        // 获取请求过来的数据JSON
        JSONObject jsonObject = JSONObject.parseObject(message.getPayload());
        log.info("TeacherHandler获取请求参数：{}", jsonObject);

        //开启定时器(每次启动任务的延迟时间为2秒，任务执行完成后的延迟时间为5秒。因此，该任务每隔5秒执行一次。)
        executorService.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                //设置返回的数据
                Map<String, String> result = new HashMap<>();
                result.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                result.put("msg", "成功");
                TextMessage textMessage = new TextMessage(JSON.toJSONString(result));
                //发送数据（这里日常处理推荐使用JSON）
                try {
                    session.sendMessage(textMessage);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }, 2, 5, TimeUnit.SECONDS);
    }

    /***
     * 在WebSocket传输错误时调用
     */
    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        log.info("handleTransportError()  TeacherHandler方法执行，在WebSocket传输错误时调用....");
        //异常后也需要关闭定时器
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    /***
     * 在WebSocket连接关闭后调用
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        log.info("afterConnectionClosed() TeacherHandler方法执行，在WebSocket连接关闭后调用....");
        //关闭定时器
        if (executorService != null) {
            executorService.shutdown();
        }
    }
}
