package cn.xw.websocket.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * 学生处理器（这里可以理解是我们创建了一个Controller）
 * WebSocket消息处理，这里以TextWebSocketHandler文本的处理方式
 *
 * @author Anhui OuYang
 * @version 1.0
 **/
@Slf4j          // 日志记录
@Component      // 注册到容器
public class StudentHandler extends TextWebSocketHandler {

    /***
     * 在WebSocket连接建立后调用
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.info("afterConnectionEstablished() StudentHandler方法执行，在WebSocket连接建立后调用....");
        super.afterConnectionEstablished(session);
    }

    /***
     * 处理WebSocket文本消息（若处理二进制非文本数据则使用handleMessage()方法或者继承BinaryWebSocketHandler类）
     */
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        log.info("handleTextMessage() StudentHandler方法执行，处理WebSocket文本消息....");
        super.handleTextMessage(session, message);
    }

    /***
     * 在WebSocket传输错误时调用
     */
    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        log.info("handleTransportError()  StudentHandler方法执行，在WebSocket传输错误时调用....");
        super.handleTransportError(session, exception);
    }

    /***
     * 在WebSocket连接关闭后调用
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        log.info("afterConnectionClosed() StudentHandler方法执行，在WebSocket连接关闭后调用....");
        super.afterConnectionClosed(session, status);
    }
}
