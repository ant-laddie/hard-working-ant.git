package cn.xw.websocket.interceptors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

/**
 * WebSocket握手拦截器，检查握手请求和响应，对WebSocketHandler传递属性，用于区别WebSocket
 *
 * @author Anhui OuYang
 * @version 1.0
 **/
@Slf4j
@Component
public class MyWebSocketInterceptor extends HttpSessionHandshakeInterceptor {

    /***
     * 握手之前被调用
     * @param request 请求信息
     * @param response 响应信息
     * @param wsHandler 用于处理WebSocket通信过程中的各种事件和消息
     * @param attributes 如果需要，可以使用setAttribute方法添加属性，这些属性可以在后续处理中使用
     * @return 返回true表示继续握手，或者返回false以终止握手
     * @throws Exception 异常信息
     */
    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
                                   WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
        log.info("执行beforeHandshake()  请求被拦截器拦截，当前请求地址为：{}", request.getURI());
        // 获取参数请求的param参数
        MultiValueMap<String, String> params = UriComponentsBuilder.fromUri(request.getURI()).build().getQueryParams();
        log.info("=====》拦截器拦截获取当前请求的params：{}", params);
        // 获取请求头信息
        HttpHeaders headers = request.getHeaders();
        log.info("=====》拦截器拦截获取当前请求头信息：{}", headers);
        //注因为是建立连接走拦截器，后期的请求则不走拦截器了，
        return super.beforeHandshake(request, response, wsHandler, attributes);
    }

    /***
     * 握手成功之后或者失败之后被调用；可以利用这个方法去清理任何未完成的状态并记录异常。
     */
    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response,
                               WebSocketHandler wsHandler, Exception ex) {
        log.info("执行afterHandshake()  请求被拦截器放行....");
        super.afterHandshake(request, response, wsHandler, ex);
    }
}
