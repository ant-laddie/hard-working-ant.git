package cn.xw.config;

import cn.xw.config.interceptor.MyInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * MVC配置信息
 *
 * @author Anhui OuYang
 * @version 1.0
 **/
@Configuration  // 一定要配置为配置类
@RequiredArgsConstructor
public class WebMvcConfig implements WebMvcConfigurer {

    // 属性注入（使用构造器注入，通过@RequiredArgsConstructor注解生成必要的构造器）
    private final MyInterceptor myInterceptor;

    /***
     * 配置拦截器信息
     * @param registry 拦截器注册表
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // 配置myInterceptor的拦截规范（如拦截的路径等等）
        InterceptorRegistration interceptorA = registry.addInterceptor(myInterceptor);

        // 设置拦截器的配置规则
        interceptorA
                // 指定拦截器的执行顺序。值越小，越先执行拦截器(但是得整型)。
                .order(1)
                // 设置需要拦截的路径（这里拦截所有的路径）
                .addPathPatterns("/**")
                // 设置拦截器的放行资源（代表不拦截）
                // 设置登录放行
                .excludePathPatterns("/user/login","/user/jumpLogin")
                // 设置Swagger访问放行
                .excludePathPatterns("/swagger-ui.html/**", "/swagger-ui.html", "/swagger-ui.html#/**")
                // 如资源文件放行
                .excludePathPatterns("/doc.html", "classpath:/META-INF/resources/");
        // 谨慎使用放行"/**"，这代表全部放行了，那么拦截器就相当于无效配置
        //.excludePathPatterns("/**");

        // 若有多个拦截器则在下面需要配置多个(如下面interceptorB，我们需要对这个进行路径拦截的配置)
        // InterceptorRegistration interceptorB = registry.addInterceptor(自定义的拦截器对象);
    }

    /***
     * 配置全局跨域处理
     * @param registry CORS跨域注册表
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 配置全局跨域信息
        registry
                // 添加映射路径（凡是在addMapping配置的路径则代表可以跨域访问）
                .addMapping("/demo/**")
                // 设置放行哪些域 SpringBoot2.4.4下低版本使用.allowedOrigins("*")
                //.allowedOrigins("*")
                .allowedOriginPatterns("*")
                // 是否允许跨域请求携带凭证Cookie发送
                .allowCredentials(true)
                // 放行哪些请求方式，也可以使用.allowedMethods("*")放行全部
                .allowedMethods("GET", "POST")
                // 放行哪些请求头部信息
                .allowedHeaders("*")
                // 暴露哪些响应头部信息
                .exposedHeaders("*")
                // 设置响应的缓存时间
                .maxAge(1800);
        // 若存在多个跨域则可以设置多个registry；"/**"代表所以都不跨域，相当设置这个，其它都没必要设置了
        // registry.addMapping("/**").allowedOriginPatterns("*");
    }
}
