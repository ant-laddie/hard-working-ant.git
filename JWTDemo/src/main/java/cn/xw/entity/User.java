package cn.xw.entity;

import lombok.Data;

/**
 * 用户实体
 *
 * @author Anhui OuYang
 * @version 1.0
 **/
@Data
public class User {
    private Integer id;         // id
    private String name;        // 姓名
    private Integer age;        // 年龄
    private String address;     // 地址
    private String account;     // 账号
    private String password;    // 密码

    public User(Integer id, String name, Integer age, String address, String account, String password) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
        this.account = account;
        this.password = password;
    }
}
