package cn.xw.utils.security.rsa;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;
import java.security.cert.Certificate;
import java.util.Date;

/**
 * 证书信息
 *
 * @author Anhui AntLaddie（博客园蚂蚁小哥）
 * @version 1.0
 **/
@Data
public class CertificateMessage {

    /***
     * 证书别名
     */
    private String certificateAlias;

    /***
     * 证书版本号
     */
    private Integer version;

    /***
     * 证书序列号
     */
    private BigInteger serialNumber;

    /***
     * 证书颁发者
     */
    private IssuerMessage issuer;

    /***
     * 证书主题
     */
    private IssuerMessage subject;

    /***
     * 证书公钥
     */
    private String publicKey;

    /***
     * 证书公钥算法类型
     */
    private String publicKeyAlgorithm;

    /***
     * 证书签名算法
     */
    private String signatureAlgorithm;

    /***
     * 证书创建时间
     */
    private Date notBefore;

    /***
     * 证书过期时间
     */
    private Date notAfter;

    /***
     * 证书原生对象信息（方便操作对象）
     */
    private Certificate certificate;

    /***
     * 颁发者信息或者主题信息
     */
    @Getter
    @Setter
    public static class IssuerMessage {
        private String country = "";            // 国家
        private String province = "";           // 州或省份
        private String city = "";               // 城市
        private String organization = "";       // 组织
        private String department = "";         // 部门
        private String company = "";            // 公司
    }
}
