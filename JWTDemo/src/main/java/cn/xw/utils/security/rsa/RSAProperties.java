package cn.xw.utils.security.rsa;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 关于RSA的生成密钥对配置信息（使用前请创建RSAProperties对象）
 * 生成密钥对和加密解密时请使用相同配置完成
 *
 * @author Anhui AntLaddie（博客园蚂蚁小哥）
 * @version 1.0
 */
@Data
public class RSAProperties {

    // ================================== 关于加密算法的配置信息 ==================================
    public String keyStoreType = "PKCS12";                 // 证书类型，默认PKCS12，如可选：JKS
    public static final String KEY_ALGORITHM = "RSA";      // 加解密算法关键字，如：RSA、DSA、EC...
    public static final String ENCRYPTION_ALGORITHM_NAME = "RSA/ECB/PKCS1Padding"; // 加密算法名称
    private Integer modulus = 2048; // 设置生成的密钥对所使用的模数的位数，默认2048(最小512)，越大越安全，但性能差

    // ================================== 关于生成的文件存储配置 ==================================
    private String datePath = sdfA.format(new Date()) + "/";   // 构建细分路径，如：xxx/202306151820
    public static final String WRITE_PATH = "security/rsa/";   // 数字证书、密钥、公钥公共存储位置
    private String keystoreInfoFileName = "certificate.p12";   // 密钥库文件名称，内包含证书和私钥
    public static final String PUB_FILE_NAME = "public_key.pem";           // 公钥文件名称
    public static final String PRI_FILE_NAME = "private_key.pem";          // 私钥文件名称
    public static final String DIGITAL_CERT = "digital_certificate.pem";   // 数字证书文件名称

    // ================================== 关于密钥库一些信息配置 ==================================
    private String priAlias = "myPrivateKeyAlias";              // 私钥别名信息
    private String certificateAlias = "myCertificateAlias";     // 证书别名信息
    private String keyStorePassword = "##ED12^a*8(&E6^PR";      // 设置生成的密钥库密码
    private String privateKeyPassword = "h3J^*a#12@Klp$%z";     // 设置密钥库内的私钥密码

    // ================================== 关于数字证书的相关配置 ==================================
    private Integer certificateExpTime = 60 * 24 * 365;     // 证书过期时间/分钟（一年时间365天）
    private String country = "";                        // 国家       如：中华人民共和国(测试)
    private String province = "";                       // 州或省份    如：安徽省(测试)
    private String city = "";                           // 城市       如：六安市(测试)
    private String organization = "";                   // 组织       如：xx科技有限公司(测试)
    private String department = "";                     // 部门       如：研发部(测试)
    private String company = "";                        // 公司       如：xx分公司(测试)

    // ================================== 其它的一些基础配置信息 ==================================
    // 是否为简单模式：
    //      标准模式 true：生成密钥库、证书和公钥（其中密钥库存在密码，密钥库内的私钥存在密码；而公钥则是一个xxx.pem的文件）
    //      简单模式 false：生成数字证书、私钥、公钥（不安全，其中这些文件都是xxx.pem文件）
    private Boolean simpleGeneration = true;

    private static final SimpleDateFormat sdfA = new SimpleDateFormat("yyyyMMddHHmm");

    /***
     * 设置证书基本主题信息；设置顺序为：国家、州或省份、城市、组织、部门、公司
     */
    public void setCertificateInfo(String country, String province, String city, String organization,
                                   String department, String company) {
        this.country = country;
        this.province = province;
        this.city = city;
        this.organization = organization;
        this.department = department;
        this.company = company;
    }
}
