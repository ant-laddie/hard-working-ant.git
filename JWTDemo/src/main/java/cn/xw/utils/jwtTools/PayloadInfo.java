package cn.xw.utils.jwtTools;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * JWT的载荷信息（JWT封装的载荷信息都在里面）
 * 前七个字段不是必须的，推荐存在，但是最后一个claim字段必须存在，这个是保存具体token核心载荷数据
 * 具体参考：<a href="https://datatracker.ietf.org/doc/html/rfc7519#section-4.1">参考地址</a>
 *
 * @author AnHui OuYang
 * @version 1.0
 */
@Data
public class PayloadInfo<T> {

    /***
     * Issuer（发行者）：表示 JWT 的签发者。(发行方 一般为项目地址前缀)
     */
    private String iss;

    /***
     * Subject（主题）：表示 JWT 所面向的用户，即该 JWT 的主题。
     */
    private String sub;

    /***
     * Audience（听众）：表示接收 JWT 的一方的标识，即该 JWT 的预期观众。
     * （接收方，一个或多个 一般为项目地址前缀）
     */
    private List<String> aud;

    /***
     * Expiration Time（过期时间）：以数字时间戳形式指定 JWT 的过期时间，
     * 这个时间必须在当前时间之后，否则该 JWT 将被拒绝。
     */
    private Date exp;

    /***
     * Not Before（生效时间）：以数字时间戳形式指定 JWT 开始生效的时间。
     * 在此时间之前验证 JWT 会被拒绝，建议将其设置为稍早于当前时间的时刻。
     */
    private Date nbf;

    /***
     * Issued At（发布时间）：以数字时间戳形式指定 JWT 的发布时间，
     * 用于侦测已发表的声明的加入延迟超过了某些门限值的情况。
     */
    private Date iat;

    /***
     * JWT ID（JWT ID）：为 JWT 提供一个唯一的标识符。
     * 这个标识符可以用来防止 JWT 被重播攻击。
     */
    private String jti;

    /***
     * 核心载荷数据，如登录成功生成Token则可以存放User信息
     */
    private T claim;
}
