package cn.xw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenApi3DemoSwagger3Application {

    public static void main(String[] args) {
        SpringApplication.run(OpenApi3DemoSwagger3Application.class, args);
    }

}
